package com.meridianid.farizramadhan.belajarmvpdaggerretrofit.presenter;

import android.app.Application;

import com.meridianid.farizramadhan.belajarmvpdaggerretrofit.presenter.component.DaggerNetworkComponent;
import com.meridianid.farizramadhan.belajarmvpdaggerretrofit.presenter.component.NetworkComponent;
import com.meridianid.farizramadhan.belajarmvpdaggerretrofit.presenter.module.AppModule;
import com.meridianid.farizramadhan.belajarmvpdaggerretrofit.presenter.module.NetModule;

/**
 * Created by fariz ramadhan.
 * website : www.farizdotid.com
 * github : https://github.com/farizdotid
 */


public class App extends Application {

    private NetworkComponent mNetworkComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mNetworkComponent = DaggerNetworkComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule("https://api.github.com/"))
                .build();
    }

    public NetworkComponent getNetworkComponent() {
        return mNetworkComponent;
    }
}
