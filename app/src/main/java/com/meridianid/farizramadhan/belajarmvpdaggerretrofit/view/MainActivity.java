package com.meridianid.farizramadhan.belajarmvpdaggerretrofit.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.meridianid.farizramadhan.belajarmvpdaggerretrofit.R;
import com.meridianid.farizramadhan.belajarmvpdaggerretrofit.model.Results;
import com.meridianid.farizramadhan.belajarmvpdaggerretrofit.presenter.App;
import com.meridianid.farizramadhan.belajarmvpdaggerretrofit.presenter.apiservice.BaseApiService;
import com.meridianid.farizramadhan.belajarmvpdaggerretrofit.presenter.apiservice.UtilsApi;

import java.util.List;

import javax.inject.Inject;

import butterknife.internal.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    BaseApiService mApiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((App) getApplication()).getNetworkComponent().inject(this);

        mApiService = UtilsApi.getAPIService();
        mApiService.getUser("farizdotid").enqueue(new Callback<Results>() {
            @Override
            public void onResponse(Call<Results> call, Response<Results> response) {
                Log.i("debug", "onResponse: BERHASIL");
                Log.i("debug", "onResponse: NAMA > " + response.body().getName());
            }

            @Override
            public void onFailure(Call<Results> call, Throwable t) {
                Log.e("debug", "onFailure: ERROR > " + t.getMessage());
            }
        });
    }

}
