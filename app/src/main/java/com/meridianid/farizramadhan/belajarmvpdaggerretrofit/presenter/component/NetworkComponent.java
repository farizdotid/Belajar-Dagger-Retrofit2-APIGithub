package com.meridianid.farizramadhan.belajarmvpdaggerretrofit.presenter.component;

import com.meridianid.farizramadhan.belajarmvpdaggerretrofit.presenter.module.AppModule;
import com.meridianid.farizramadhan.belajarmvpdaggerretrofit.presenter.module.NetModule;
import com.meridianid.farizramadhan.belajarmvpdaggerretrofit.view.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by fariz ramadhan.
 * website : www.farizdotid.com
 * github : https://github.com/farizdotid
 */


@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface NetworkComponent {
    void inject(MainActivity activity);
}
