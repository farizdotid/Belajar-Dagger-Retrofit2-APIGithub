package com.meridianid.farizramadhan.belajarmvpdaggerretrofit.presenter.apiservice;

/**
 * Created by fariz ramadhan.
 * website : www.farizdotid.com
 * github : https://github.com/farizdotid
 */


public class UtilsApi {
    public static final String BASE_URL_API = "https://api.github.com/";

    public static BaseApiService getAPIService(){
        return RetrofitClient.getClient(BASE_URL_API).create(BaseApiService.class);
    }
}
