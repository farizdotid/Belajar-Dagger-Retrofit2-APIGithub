package com.meridianid.farizramadhan.belajarmvpdaggerretrofit.presenter.apiservice;

import com.meridianid.farizramadhan.belajarmvpdaggerretrofit.model.Results;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by fariz ramadhan.
 * website : www.farizdotid.com
 * github : https://github.com/farizdotid
 */


public interface BaseApiService {

    @GET("users/{username}")
    Call<Results> getUser(@Path("username") String username);

}
